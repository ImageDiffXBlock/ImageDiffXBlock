Image Difference Assignment XBlock
==============================


Installation
------------


~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

0. If there is no running course using the component, one can uninstall existing version of the Package

   - ``Check for directory called xblimagediff_xblock-0.1-py2.7.egg-info in /edx/app/edxapp/venvs/edxapp/local/lib/python2.7/site-packages/``

   - ``If it is present, delete it``

   - ``Check if there is a directory called xblimagediff in /edx/app/edxapp/venvs/edxapp/local/lib/python2.7/site-packages/``

   - ``If it is present, delete it``

   - ``Then install the package as given in Step 1. Install Package

   However, if there is a running course which is using the component and a new version of the same is to be deployed, then the suggested approach is as follows

   - ``Get only the modified source code files from git to a temporary directory``
   - ``Overwrite the files in /edx/app/edxapp/venvs/edxapp/local/lib/python2.7/site-packages/xblimagediff directory with the modified source code files``
   - ``Restart the LMS and CMS servers``

1. Install Package 


   -  ``Clone the branch http://gitlab.cse.iitb.ac.in/ImageDiffXBlock/ImageDiffXBlock.git -b ImageDiffXBlock``
   -  ``Run command sudo -u edxapp /edx/bin/pip.edxapp install /path/to/your/block`` 
   -  ``If the environment supports S3 then in the lms.env.json file add the entry FILE_STORE and set it to S3 as follows : "FILE_STORE": "S3",
e.g. If the XBlock is cloned in home directory, it will create a directory structure as /home/<username>/ImageDiffXBlock/xblimagediff/xblimagediff



2. Check if xblimagediff is added in installed Django apps. If not, please raise this with Dev Team.

   - In ``/edx/app/edxapp/edx-platform/lms/envs/common.py`` and ``/edx/app/edxapp/edx-platform/cms/envs/common.py``, add in ``INSTALLED_APPS``

          xblimagediff


   - In ``/edx/app/edxapp/edx-platform/cms/envs/common.py``, check if following parameter is set. If not, please raise this with Dev Team.

          "ALLOW_ALL_ADVANCED_COMPONENTS": True,



3. Restart LMS and CMS servers


Course Authoring in edX Studio
------------------------------

1. Change Advanced Settings

   1. Open a course you are authoring and select "Settings" ⇒ "Advanced
      Settings
   2. Navigate to the section titled "Advanced Module List"
   3. Add "xblimagediff" to module list.
   
   
2. Create an ImageDiffXblock XBlock

   1. Return to the Course Outline
   2. Create a Section, Sub-section and Unit, if you haven't already
   3. In the "Add New Component" interface, you should now see an "Advanced" 
      button
