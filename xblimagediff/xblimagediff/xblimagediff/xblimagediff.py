"""TO-DO: Write a description of what this XBlock is."""

import datetime
import hashlib
import json
import logging
import mimetypes
import os
import pkg_resources
import pytz

# IITBombayX zip
import zipfile
from django.conf import settings
from itertools import izip
import io
import shutil
import math
import re
import json, ast
import sys


from functools import partial

from courseware.models import StudentModule

from django.core.exceptions import PermissionDenied
from django.core.files import File
from django.core.files.storage import default_storage
from django.conf import settings
from django.template import Context, Template

from student.models import user_by_anonymous_id
#from submissions import api as submissions_api
#from submissions.models import StudentItem as SubmissionsStudent

from webob.response import Response

from xblock.core import XBlock
from xblock.exceptions import JsonHandlerError
from xblock.fields import DateTime, Scope, String, Float, Integer, Dict, List,Boolean
from xblock.fragment import Fragment

from xmodule.util.duedate import get_extended_due_date
from imagecomp2 import imagecomp,createwatermark
from xmodule.contentstore.content import StaticContent
from file_storage import save_file
from xmodule.capa_base_constants import SHOWANSWER
from django.utils.timezone import UTC
from xmodule.contentstore.django import contentstore
from opaque_keys.edx.keys import CourseKey, AssetKey
from lms.envs import aws
from storages.backends.s3boto import S3BotoStorageFile

#from django.http import HttpResponseNotFound
#from django.http import Http404

import grades


log = logging.getLogger(__name__)
BLOCK_SIZE = 8 * 1024
FORCE_EVAL_EVERYTIME = True
#IITBombayX- Directory changed from uploads to media
IMAGEDIFF_ROOT = "/edx/var/edxapp/media/"

def reify(meth):
    """
    Decorator which caches value so it is only computed once.
    Keyword arguments:
    inst
    """
    def getter(inst):
        """
        Set value to meth name in dict and returns value.
        """
        value = meth(inst)
        inst.__dict__[meth.__name__] = value
        return value
    return property(getter)

class ImageDiffXBlock(XBlock):
    """
    This XBlock is a new assessment type in IITBombayX. The faculty will upload images as course assets and configure the XBlock to compare 
    student submission images with the course asset image. Depending on a threshold value of number of differing pixels, the student will 
    be given grades.
    """
    has_score = True
    icon_class = 'problem'
    STUDENT_FILEUPLOAD_MAX_SIZE = 4 * 1000 * 1000  # 4 MB

    display_name = String(
	display_name = "Name",
        default='Image Difference Assignment', scope=Scope.settings,
        help="This name appears in the horizontal navigation at the top of "
             "the page."
    )

    staff_score = Integer(
        display_name="Score assigned by non-instructor staff",
        help=("Score will need to be approved by instructor before being "
              "published."),
        default=None,
        scope=Scope.settings
    )

    comment = String(
        display_name="Instructor comment",
        default='',
        scope=Scope.user_state,
        help="Feedback given to student by instructor."
    )

    annotated_sha1 = String(
        display_name="Annotated SHA1",
        scope=Scope.user_state,
        default=None,
        help=("sha1 of the annotated file uploaded by the instructor for "
              "this assignment.")
    )

    annotated_filename = String(
        display_name="Annotated file name",
        scope=Scope.user_state,
        default=None,
        help="The name of the annotated file uploaded for this assignment."
    )

    annotated_mimetype = String(
        display_name="Mime type of annotated file",
        scope=Scope.user_state,
        default=None,
        help="The mimetype of the annotated file uploaded for this assignment."
    )

    annotated_timestamp = DateTime(
        display_name="Timestamp",
        scope=Scope.user_state,
        default=None,
        help="When the annotated file was uploaded")

  #IITBombayX 
    question = String(
        display_name="Question",
        default='', scope=Scope.settings,
        help="This question statement appears for the Student "
             
    )
    

    
    threshold_unit = String(
        display_name="Threshold unit",
        scope=Scope.settings,
        default=None,
	help="Unit of the upper limit of error upto which full score will be given.",
        values=[
                 {"display_name": "%", "value": "0"},
                 {"display_name": "Pixels", "value": "1"},
               ]

    )
    threshold_value = Float(
        display_name="Threshold value",
        help=("Value of the upper limit of error upto which full score will be given."),
        default=None,
        scope=Scope.settings
    )

    file_map = Dict(
        default={"file":[]},
        scope=Scope.settings
    )

    weight = Float(
        display_name="Problem Weight",
        help=("Defines the number of points each problem is worth. "
              "If the value is not set, the problem is worth the sum of the "
              "option point values."),
        values={"min": 0, "step": .1},
	default=1.0,
        scope=Scope.settings
    )

    partial_score_grade=Dict(
        default={},
        scope=Scope.user_state
    )
     
    partial_score_absolute=Dict(
       default={},
       scope=Scope.user_state
    )



    image_passed=Integer(
       default=0,
       scope=Scope.user_state
    )

    showanswer = String(
        display_name=("Show Answer"),
        help=("Defines when to show the answer to the problem."),
        scope=Scope.settings,
        default=SHOWANSWER.FINISHED,
        values=[
            {"display_name": ("Always"), "value": SHOWANSWER.ALWAYS},
            #{"display_name": ("Answered"), "value": SHOWANSWER.ANSWERED},
            {"display_name": ("Attempted"), "value": SHOWANSWER.ATTEMPTED},
            {"display_name": ("Closed"), "value": SHOWANSWER.CLOSED},
            {"display_name": ("Finished"), "value": SHOWANSWER.FINISHED},
            {"display_name": ("Correct or Past Due"), "value": SHOWANSWER.CORRECT_OR_PAST_DUE},
            {"display_name": ("Past Due"), "value": SHOWANSWER.PAST_DUE},
            {"display_name": ("Never"), "value": SHOWANSWER.NEVER}]
    )
   
    attempts = Integer(
        help=("Number of attempts taken by the student on this problem."),
        default=0,
        scope=Scope.user_state)
    max_attempts = Integer(
        display_name=("Maximum Attempts"),
        help=("Defines the number of times a student can try to answer this problem. "
               "If the value is not set, infinite attempts are allowed."),
        scope=Scope.settings
    )
    #done = Boolean(help=("Whether the student has answered the problem"), scope=Scope.user_state)
 

    #total_pixel=List(
    #         default=[],
    #         scope=Scope.user_state
    #)

    # added xbl
    raw_answer=Dict(
       default={},
       scope=Scope.user_state
    )

    # added xbl 
    points_earned = Float(
        help=("Grade of the student on this problem."),
        default=None,
        scope=Scope.user_state)

    # added xbl
    points_possible = Float(
        help=("Max_Grade of the student on this problem."),
        default=None,
        scope=Scope.user_state)


    def max_score(self):
        """
        Return the maximum score possible.
        """
	return len(self.file_map["file"])
        #return 100

    @reify
    def block_id(self):
        """
        Return the usage_id of the block.
        """
        return self.scope_ids.usage_id

    """
    del xbl

    def student_submission_id(self, submission_id=None):
        # pylint: disable=no-member
        '''
        Returns dict required by the submissions app for creating and
        retrieving submissions for a particular student.
        '''
        if submission_id is None:
            submission_id = self.xmodule_runtime.anonymous_student_id
            assert submission_id != (
                'MOCK', "Forgot to call 'personalize' in test."
            )
        return {
            "student_id": submission_id,
            "course_id": self.course_id,
            "item_id": self.block_id,
            "item_type": 'sga',  # ???
        }
    """

    # add xbl
    def get_submission(self):
        """
        Get student's present submission.
        """
        submissions = self.raw_answer
        if submissions is not None:
            return {
                "answer": self.raw_answer
            }
 
    """    
    del xbl

    def get_submission(self, submission_id=None):
        '''
        Get student's most recent submission.
        '''
        submissions = submissions_api.get_submissions(
            self.student_submission_id(submission_id))
        if submissions:
            # If I understand docs correctly, most recent submission should
            # be first
            return submissions[0]
    """

    # add xbl
    def get_score(self):
        '''
        Return student's current score.
        '''
        score = self.points_earned
        if score is not None:
            return score


    """     
    del xbl 
    def get_score(self, submission_id=None):
        '''
        Return student's current score.
        '''
        score = submissions_api.get_score(
            self.student_submission_id(submission_id)
        )
        if score:
            return score['points_earned']
    """

    @reify
    def score(self):
        """
        Return score from submissions.
        """
        return self.get_score()

    def student_view(self, context=None):
        # pylint: disable=no-member
        """
        The primary view of the StaffGradedAssignmentXBlock, shown to students
        when viewing courses.
        """
        context = {
            "student_state": json.dumps(self.student_state()),
            "id": self.location.name.replace('.', '_'),
            "location" : self.location,
            "max_file_size": getattr(
                settings, "STUDENT_FILEUPLOAD_MAX_SIZE",
                self.STUDENT_FILEUPLOAD_MAX_SIZE
                
            )
        }
	#IITBombayX 
	#Uncomment this code if Staff Debug Info option is to be provided. The rescore-problem method will have to be implemented for the class.
        #if self.show_staff_grading_interface():
        #    context['is_course_staff'] = self.is_course_staff()
        #    self.update_staff_debug_context(context)

        fragment = Fragment()
        fragment.add_content(
            render_template(
                'templates/imagediff/show.html',
                context
            )
        )
        fragment.add_css(_resource("static/css/xblimagediff.css"))
        fragment.add_javascript(_resource("static/js/src/xblimagediff.js"))
        fragment.add_javascript(_resource("static/js/src/jquery.tablesorter.min.js"))
        fragment.initialize_js('ImageDiffXBlock')
        return fragment

    @classmethod
    def parse_xml(self, node, runtime, keys, id_generator):
        ret = super(ImageDiffXBlock, self).parse_xml(node, runtime, keys, id_generator)
        if "file_map" in node.attrib:
            ret.file_map = json.loads(node.attrib['file_map'].replace("u'", "'").replace("'", '"'))

        return ret

    def update_staff_debug_context(self, context):
        # pylint: disable=no-member
        """
        Add context info for the Staff Debug interface.
        """
        published = self.start
        context['is_released'] = published and published < _now()
        context['location'] = self.location
        context['category'] = type(self).__name__
        context['fields'] = [
            (name, field.read_from(self))
            for name, field in self.fields.items()]


    def student_state(self):
        """
        Returns a JSON serializable representation of student's state for
        rendering in client view.
        """
        submission = self.get_submission()
        if submission:
            # del xbl
            #uploaded = {"filename": submission['answer']['filename']}
            # add xbl
            uploaded_submission = submission.get("answer").get("filename", None)
            if uploaded_submission:
                uploaded = {"filename": submission['answer']['filename']}
            else:
                uploaded = None
        else:
            uploaded = None

        if self.annotated_sha1:
            annotated = {"filename": self.annotated_filename}
        else:
            annotated = None

        score = self.score
        if score is not None:
            graded = {'score': score, 'comment': self.comment}
        else:
            graded = None

        student_image=["?image="+self.file_map['file'][i][1]+"&type=upload" for i in range (len(self.file_map['file']))]
        solution_image=["?image="+"watermark-"+self.file_map['file'][i][1]+"&type=solution" for i in range (len(self.file_map['file']))]
        diff_image=["?image="+self.file_map['file'][i][1]+"&type=difference" for i in range (len(self.file_map['file']))]
        return {
            "display_name": self.display_name,
            "uploaded": uploaded,
            "annotated": annotated,
            "graded": graded,
            "max_score": self.max_score(),
            "closed": self.closed(),
            "question":self.question,
            "threshold_unit":self.threshold_unit,
            "threshold":self.threshold_value,
	    "weight":self.weight,
            "file_map":self.file_map['file'],
            "partial_score_grade":self.partial_score_grade,
            "partial_score_absolute":self.partial_score_absolute,
            "image_passed":self.image_passed,
	    "is_course_staff": self.is_course_staff(),
            "answer_available": self.answer_available(),
            "attempts":self.attempts,
            "raw_answer":self.raw_answer,
            "points_possible":self.points_possible,
            "points_earned":self.points_earned,
            "max_attempts":self.max_attempts,
            "student_image":student_image,
            "solution_image":solution_image,
            "diff_image":diff_image
	    #"total_pixel": self.total_pixel

        }


    def studio_view(self, context=None):
        """
        Return fragment for editing block in studio.
        """
        try:
            cls = type(self)

            def none_to_empty(data):
                """
                Return empty string if data is None else return data.
                """
                return data if data is not None else ''
            edit_fields = (
                (field, none_to_empty(getattr(self, field.name)), validator)
                for field, validator in (
                    (cls.display_name, 'string'),
                    (cls.question,'string'),
                    (cls.threshold_unit ,'string'),
                    (cls.threshold_value,'integer'),
                    (cls.showanswer,'string'),
                    (cls.max_attempts,'integer'))
                 
            )

            context = {
                'fields': edit_fields,
		'elements' :self.file_map['file']
            }
            fragment = Fragment()
            fragment.add_content(
                render_template(
                    'templates/imagediff/edit.html',
                    context
                )
            )
            fragment.add_javascript(_resource("static/js/src/studio.js"))
            fragment.initialize_js('ImageDiffXBlock')
            return fragment
        except:  # pragma: NO COVER
            log.error("Don't swallow my exceptions", exc_info=True)
            raise

    @XBlock.json_handler
    def save_imagediffconfig(self, data, request,suffix=''):
        # pylint: disable=unused-argument
        """
        Persist block data when updating settings in studio.
        """
        self._validate_data(data)
        self.display_name=data.get('display_name',self.display_name)
        self.question=data.get('question',self.question)
        self.threshold_unit=data.get('threshold_unit',self.threshold_unit)
        self.threshold_value=data.get('threshold_value',self.threshold_value)
        #self.weight = data.get('weight', self.weight)
        if data.get('max_attempts'):
            self.max_attempts=data.get('max_attempts')
        else:
            self.max_attempts=100000000 
        self.showanswer=data.get('showanswer',self.showanswer)
        #self.done=False
        self.file_map={"file":data.get('filearr')}
        
        self.weight = data.get('weight', self.max_score())
        log.info(self)
       
    def _validate_data(self,data):
        #VALIDATION OF display_name question is not defined 
        display_name = data.get('display_name', self.display_name)
        if display_name.isspace() or not re.match(r'\w', display_name):
            raise JsonHandlerError(400, 'Name cannot be all spaces or start with space')
        question = data.get('question', self.question)
        if question.isspace() or not re.match(r'\w', question):
            raise JsonHandlerError(400, 'Question cannot be all spaces or start with space')
        threshold_unit =data.get('threshold_unit',self.threshold_unit)
        if threshold_unit == "0":
            try:
                threshold_value = float(data.get('threshold_value',self.threshold_value))
            except ValueError:
                raise JsonHandlerError(400, 'Threshold Value  must be a  number')
            # Check that we are positive
            if threshold_value < 0 or threshold_value > 100.0:
                raise JsonHandlerError(
                    400, 'Threshold Value must be a positive decimal number less than or equal to 100.0'
                )
        elif threshold_unit == "1":
            try:
                threshold_value = int(data.get('threshold_value',self.threshold_value))
            except ValueError:
                raise JsonHandlerError(400, 'Threshold Value  must be a integer number')
 
            if threshold_value < 0:
                raise JsonHandlerError(
                    400, 'Threshold Value must be a positive integer number'
                )
        else:
            pass
    
        try:
            if data.get('max_attempts'):
                max_attempts = int(data.get('max_attempts'))
            else:
                max_attempts=data.get('max_attempts')
        except ValueError:
            raise JsonHandlerError(400, 'Maximum Attempts must be a integer number')
        if max_attempts and max_attempts < 0:
            raise JsonHandlerError(
                    400, 'Maximum Attempts must be a positive integer number'
                )
 
        for filepair in data.get('filearr'):
            asset_file=filepair[0]
            student_file=filepair[1]
            if asset_file.strip()=='' or student_file.strip()=='':
                raise JsonHandlerError(400, 'Asset or Student file cannot be blank')
            try:
                asset_file_parts=asset_file.split('.')
                student_file_parts=student_file.split('.')
                asset_prefix = asset_file_parts[0]
                asset_extn = asset_file_parts[1]
                student_prefix = student_file_parts[0]
                student_extn = student_file_parts[1]
            except Exception as e:
                log.info(e)
                raise JsonHandlerError(400, 'Both the filenames, {0} and {1} must contain an extension.'.format(asset_file,student_file))
            if asset_prefix.isspace() or student_prefix.isspace() :
                raise JsonHandlerError(
                    400, 'File name cannot contain only extension'
                )
            elif asset_extn!=student_extn:
                raise JsonHandlerError(
                    400, 'File extension of {0} and {1} should be same.'.format(asset_file,student_file)
                )
            url=StaticContent.convert_legacy_static_url_with_course_id(asset_file, self.course_id)
            try:
		#IITBombayX- Added check for supporting both course formats(draft and split) 
                url=url.split('/')[1] if "asset-v1:" in url else url
                asset_key = AssetKey.from_string(url) if url else None
                content=contentstore().find(asset_key)
            except:
               raise JsonHandlerError(
                    400, 'Asset {0} not found'.format(asset_file)
                )


                

  
    # IITBombayX zip
    def _unzip_zipfile(self, path, unzip_path_part, user_id_seed, facultyfile,filename):
        """
        Extracts the contents of zip file uploaded by student.
        """
        studentfile = dict()
        facultyfilea = facultyfile
        #if path and unzip_path_part and user_id_seed and self.get_storage_type()=='File':
        if path and unzip_path_part and user_id_seed:
            with zipfile.ZipFile(os.path.join(IMAGEDIFF_ROOT, path)) as zip_file:
                upperdirs = os.path.join(IMAGEDIFF_ROOT, unzip_path_part)
                if upperdirs and not os.path.exists(upperdirs):
                    os.makedirs(upperdirs)
		elif os.path.exists(upperdirs):
		    filelist = [ f for f in os.listdir(upperdirs)] 
		    for f in filelist:
                     pass
		    #os.remove(f)
		
                for zname in zip_file.namelist():
                    zfilename = os.path.basename(zname)
                    # skip directories
                    # extract valid student solution file
                    if zfilename and (facultyfilea.get(zname.split('/')[-1], None) != None):
                        # copy file (taken from zipfile's extract)
                        source = zip_file.open(zname)
                        target = file(os.path.join(IMAGEDIFF_ROOT, unzip_path_part, zfilename), "wb")
                        with source, target:
                            shutil.copyfileobj(source, target)
                            target.close()
                            if self.get_storage_type()=='S3':
                                 target=open(os.path.join(IMAGEDIFF_ROOT, unzip_path_part, zfilename), "r")
                                 file_name=os.path.join(unzip_path_part,zfilename)
                                 save_file(file_name,File(target))
                                 target.close()
                                 os.remove(os.path.join(IMAGEDIFF_ROOT, unzip_path_part, zfilename))
                                 # need to remove the file as well
    # IITBombayX zip
    def _file_score(self, facultyfile, path, answer):
        """
        Compares the faculty uploaded solution images and student solution files and returns array of scores, Passed/Failed.
	Single file upload and zip upload are handled differently.
        """
        facultyfilea = facultyfile
        score = []
        partial_score_grade={}
        partial_score_absolute={}
      
        diff_directory=self.get_dir(self.get_storage_type(),False,answer['sha1'])
        watermark_directory=self.get_dir(self.get_storage_type(),True,answer['sha1']) 
        student_single_file =  False 
        if not path.endswith(".zip") :
	    student_single_file = True 
        if len(facultyfilea) == 1 and not path.endswith(".zip") :
            student_single_file =  True
	#If the name of file uploaded by student does not match the list of expected file names
        if student_single_file and facultyfilea.get(answer['filename'], None) == None and self.get_storage_type()=='File':
            """ returns 0 score
            """
            self.delete_old_student_files(diff_directory,facultyfilea)
            return self.getscore(self.get_storage_type(), facultyfilea,partial_score_grade,partial_score_absolute,score,answer) 
        elif student_single_file and facultyfilea.get(answer['filename'], None) == None and self.get_storage_type()=='S3':
            self.delete_old_student_files(diff_directory,facultyfilea)
            return self.getscore(self.get_storage_type(), facultyfilea,partial_score_grade,partial_score_absolute,score,answer) 


        for facfile in facultyfilea:
            if student_single_file and answer['filename']==facfile:
            # Student uploaded file name matches the expected file name(as specified by faculty)
                astudentfileurl = os.path.join(IMAGEDIFF_ROOT, path) if self.get_storage_type()=='File' else path if self.get_storage_type()=='S3' else None
            else:
            # Student uploaded file name DOES NOT match the expected file name(as specified by faculty)
                astudentfileurl = os.path.join(IMAGEDIFF_ROOT, self._file_storage_unzip_and_diff_path_part(answer['sha1'], "unzip"), facfile) if self.get_storage_type()=='File' else os.path.join(self._file_storage_unzip_and_diff_path_part(answer['sha1'], "unzip"), facfile) if self.get_storage_type()=='S3' else None
                if (self.get_storage_type()=='File' and not os.path.exists(astudentfileurl)) or (self.get_storage_type()=='S3' and not default_storage.exists(astudentfileurl)):
                    afacultyfileurl =  facultyfilea.get(facfile)
                    self.file_processing(self.get_storage_type(),astudentfileurl,partial_score_grade, partial_score_absolute,answer,facfile,afacultyfileurl)
                    continue
            
            afacultyfileurl =  facultyfilea.get(facfile)

            """ If not watermark, path should contain student id """
            if self.get_storage_type()=='File' and not os.path.exists(diff_directory):
                os.makedirs(diff_directory)
            elif self.get_storage_type()=='S3' and not  os.path.exists(os.path.join(IMAGEDIFF_ROOT,diff_directory)):
                s3dir=os.path.join(IMAGEDIFF_ROOT,diff_directory)
                os.makedirs(s3dir)
                # Do we have to make dirs in s3??
            diffimage=os.path.join(diff_directory,facfile)
            """ For watermark image, path need not contain student id """
            if self.get_storage_type()=='File' and not os.path.exists(watermark_directory):
                os.makedirs(watermark_directory)
            elif self.get_storage_type()=='S3' and not os.path.exists(os.path.join(IMAGEDIFF_ROOT,watermark_directory)) :
                # Do we have to make dirs in s3??
                s3dir=os.path.join(IMAGEDIFF_ROOT,watermark_directory)
                os.makedirs(s3dir)            
            watermark_file_name="watermark-"+facfile
            watermarkimage=os.path.join(watermark_directory,watermark_file_name)

            afacultyfileurl= StaticContent.convert_legacy_static_url_with_course_id(afacultyfileurl,self.course_id)
	    solutionpath= self._create_solution_image_path_from_http_url(afacultyfileurl,facfile)

	    #Remove difference and watermark image created at the time of last upload.
            self.delete_files(self.get_storage_type(),diffimage)
            self.delete_files(self.get_storage_type(),watermarkimage)

	    partial_score_percentage,partial_score_pixel,pix_count=imagecomp(solutionpath,astudentfileurl,diffimage,watermarkimage)
            log.info("Scores....")
            log.info(partial_score_percentage)
            log.info(partial_score_pixel)
            if partial_score_percentage==None or partial_score_pixel==None:
	        partial_score_grade[facfile]="Failed"
                partial_score_absolute[facfile]="Nothing"
                continue
	    #If threshold is in Percentage unit
            if self.threshold_unit=="0":
                if math.ceil(partial_score_percentage)<=self.threshold_value:
                    score.append(1)
                    partial_score_grade[facfile]="Passed"
                    partial_score_absolute[facfile]=partial_score_percentage
	        else:
	           partial_score_grade[facfile]="Failed"
                   partial_score_absolute[facfile]=partial_score_percentage
            elif self.threshold_unit=="1":
	       #If threshold is in Pixel count unit
                if partial_score_pixel<=self.threshold_value:
                    score.append(1)
                    partial_score_grade[facfile]="Passed"
                    partial_score_absolute[facfile]=partial_score_pixel
	        else:
		    partial_score_grade[facfile]="Failed"
                    partial_score_absolute[facfile]=partial_score_pixel
	#self.total_pixel = [].append(list(total_pixels))
        self.partial_score_grade=partial_score_grade
        self.partial_score_absolute=partial_score_absolute
        self.image_passed=sum(1 for x in partial_score_grade.values() if x=="Passed")
	return sum(score)
    
    def get_storage_type(self):
        return  aws.ENV_TOKENS.get('FILE_STORE')  if  aws.ENV_TOKENS.get('FILE_STORE') else 'File'
    
    def get_dir(self,file_store,is_watermark,sha1):

        if file_store=='File' and not is_watermark:
            directory= os.path.join(IMAGEDIFF_ROOT,self._file_storage_unzip_and_diff_path_part(sha1, "diff"))
        elif file_store=='File' and is_watermark:
            directory= os.path.join(IMAGEDIFF_ROOT,self._file_storage_unzip_and_diff_path_part(sha1, "diff",is_watermark))
        elif file_store=='S3' and not is_watermark:
            directory= os.path.join(self._file_storage_unzip_and_diff_path_part(sha1, "diff"))
        elif file_store=='S3' and is_watermark:
             directory= os.path.join(self._file_storage_unzip_and_diff_path_part(sha1, "diff",is_watermark))
        else:
           pass
       
        return directory            
    
    @XBlock.handler
    def delete_old_student_files(self, directory,facultyfilea):
        if self.get_storage_type()=='File' and os.path.exists(directory):
            for file_object in os.listdir(directory):
                file_object_path = os.path.join(directory, file_object)
                if os.path.isfile(file_object_path):
                    os.remove(file_object_path)
                else:
                    pass
        elif self.get_storage_type()=='S3':
            for facfile in facultyfilea:
                delete_filename=os.path.join(directory,facfile)
                default_storage.delete(delete_filename) 

    @XBlock.handler
    def delete_files(self, file_store, filename):
        if self.get_storage_type()=='File' and os.path.exists(filename):
            os.remove(filename)
        elif self.get_storage_type()=='S3' and default_storage.exists(filename):
            default_storage.delete(filename) 

    @XBlock.handler
    def getscore(self, filestore, facultyfilea,partial_score_grade,partial_score_absolute,score,answer):
        for facfile in facultyfilea:
            partial_score_grade[facfile]="Failed"
            partial_score_absolute[facfile]= "Nothing"
            watermark_directory=self.get_dir(filestore,True,answer['sha1'])
            if filestore == "File":
                if not os.path.exists(watermark_directory):
                    os.makedirs(watermark_directory)
            watermark_file_name="watermark-"+facfile
            watermarkimage=os.path.join(watermark_directory,watermark_file_name)
            afacultyfileurl =  facultyfilea.get(facfile)
            afacultyfileurl= StaticContent.convert_legacy_static_url_with_course_id(afacultyfileurl, self.course_id)
            solutionpath= self._create_solution_image_path_from_http_url(afacultyfileurl,facfile)
            if filestore == "S3":
                if default_storage.exists(watermarkimage):
                    default_storage.delete(watermarkimage)
                if not os.path.exists(os.path.join(IMAGEDIFF_ROOT,watermark_directory)):
                    os.makedirs(os.path.join(IMAGEDIFF_ROOT,watermark_directory))
            elif filestore == "File":
                if os.path.exists(watermarkimage):
                    os.remove(watermarkimage)
            else:
                #Do nothing; for future requirements
                pass
              
            createwatermark(solutionpath,watermarkimage)
        self.partial_score_grade = partial_score_grade
        self.partial_score_absolute = partial_score_absolute
        self.image_passed=sum(1 for x in partial_score_grade.values() if x=="Passed")
        return sum(score)

    @XBlock.handler
    def file_processing(self, file_store,astudentfileurl,partial_score_grade, partial_score_absolute,answer,facfile,afacultyfileurl):
        watermark_directory=self.get_dir(file_store,True,answer['sha1'])
        diff_directory=self.get_dir(file_store,False,answer['sha1'])
        if self.get_storage_type()=='File' and not os.path.exists(astudentfileurl):
            if not os.path.exists(watermark_directory):
                os.makedirs(watermark_directory)
        elif self.get_storage_type()=='S3' and not default_storage.exists(astudentfileurl):
                if not os.path.exists(os.path.join(IMAGEDIFF_ROOT,watermark_directory)):
                    directory=os.path.join(IMAGEDIFF_ROOT,watermark_directory)
                    os.makedirs(directory)
                    # Do we have to make dir in s3??
                   # pass
        watermark_file_name="watermark-"+facfile
        watermarkimage=os.path.join(watermark_directory,watermark_file_name)
        #afacultyfileurl =  facultyfilea.get(facfile)
        afacultyfileurl=StaticContent.convert_legacy_static_url_with_course_id(afacultyfileurl, self.course_id)
        solutionpath= self._create_solution_image_path_from_http_url(afacultyfileurl,facfile)
        if self.get_storage_type()=='File': 
            if os.path.exists(watermarkimage):
	        os.remove(watermarkimage)
        elif self.get_storage_type()=='S3':
            if  default_storage.exists(watermarkimage):
                default_storage.delete(watermarkimage)
        createwatermark(solutionpath,watermarkimage)
        
        diffimage=os.path.join(diff_directory,facfile)
        if self.get_storage_type()=='File' :
            if os.path.exists(diffimage):
                os.remove(diffimage)
        elif self.get_storage_type()=='S3' :
            if  default_storage.exists(diffimage):
                     default_storage.delete(diffimage)
        partial_score_grade[facfile]="Failed"
        partial_score_absolute[facfile]="Nothing"

                 

    @XBlock.handler
    def upload_assignment(self, request, suffix=''):
        # pylint: disable=unused-argument
        """
        Save a students submission file.
        """
        require(not self.closed())
        upload = request.params['assignment']
        sha1 = _get_sha1(upload.file)
        answer = {
            "sha1": sha1,
            "filename": upload.file.name,
            "mimetype": mimetypes.guess_type(upload.file.name)[0],
        }
        # del xbl
        #student_id = self.student_submission_id()
        
        #add xbla update IITBsub
        self.raw_answer = answer 

        # IITBombayX zip changes
        #submis = submissions_api.create_submission(student_id, answer)
        path = self._file_storage_path(sha1, upload.file.name)
	if self.get_storage_type()=="File" or path.endswith(".zip"):
            filepathexists=os.path.join(IMAGEDIFF_ROOT, path)
            file_exists=os.path.exists(filepathexists)
        elif self.get_storage_type()=="S3" and not path.endswith(".zip"):
            file_exists=default_storage.exists(path)   
        if  not file_exists:
            save_file(path, File(upload.file))
            file_exists=True

 
        # IITBombayX zip starts
        if FORCE_EVAL_EVERYTIME:
            facultyfile = dict()
            for afileurl, afilename in self.file_map['file']:
                facultyfile[afilename] = afileurl

 
            if path.endswith(".zip") and file_exists:
                unzip_path_part = self._file_storage_unzip_and_diff_path_part(sha1, "unzip")
                self._unzip_zipfile(path, unzip_path_part, self.xmodule_runtime.user_id, facultyfile,File(upload.file))

            student_score = self._file_score(facultyfile, path, answer)
            if student_score != None:
                # del xbl
                #student_uuid = submis['uuid']
		try:
                   # del xbl
                   #submissions_api.set_score(student_uuid, student_score, self.max_score())

                   #add xbla update IITBsub
                   log.info("-------")
                   log.info(str(self))
                    
                   self.points_earned = float(student_score)
                   self.points_possible = float(self.max_score())

		except:
		   raise 
            if path.endswith(".zip") and os.path.exists(os.path.join(IMAGEDIFF_ROOT, path)):
                self.delete_old_files(sha1)


            # IITBombayX zip ends
            #self.done=True
            self.attempts=self.attempts+1
            # add xbl
            self.save()
            grades.student_grade_save(self.course_id, self.xmodule_runtime.user_id, self.location)

        return Response(json_body=self.student_state())


    @XBlock.handler
    def download_assignment(self, request, suffix=''):
        # pylint: disable=unused-argument
        """
        Fetch student assignment from storage and return it.
        """

        answer = self.get_submission()['answer']
        path = self._file_storage_path(answer['sha1'], answer['filename'])
        return self.download(path, answer['mimetype'], answer['filename'])

    # IITBombayX zip 
    @XBlock.handler
    def download_solution(self, request, suffix=''):
        # pylint: disable=unused-argument
        """
        Fetch student assignment(s) from storage and return it.
        """
        answer = self.get_submission()['answer']
        image = request.params['image']        
        itype = request.params['type']
        if itype=="upload":
            path = (
                '{loc.org}/{loc.course}/unzip/{user_id_seed}/{loc.block_type}'
                '/{loc.block_id}/{sha1}/{filename}'.format(
                    loc=self.location,
                    user_id_seed=self.xmodule_runtime.user_id,
                    sha1=answer['sha1'],
                    filename=image
                )
            )
        elif itype=="solution":
            path = (
                '{loc.org}/{loc.course}/diff/{loc.block_type}'
                '/{loc.block_id}/{sha1}/{filename}'.format(
                    loc=self.location,
                    user_id_seed=self.xmodule_runtime.user_id,
                    sha1=answer['sha1'],
                    filename=image
                )
            )
        elif itype=="difference":
            path = (
                '{loc.org}/{loc.course}/diff/{user_id_seed}/{loc.block_type}'
                '/{loc.block_id}/{sha1}/{filename}'.format(
                    loc=self.location,
                    user_id_seed=self.xmodule_runtime.user_id,
                    sha1=answer['sha1'],
                    filename=image
                )
            )



       # path = self._file_storage_unzip_and_diff_path(answer['sha1'], action, solution_name)

        if itype == "upload" and answer['mimetype'] != 'application/zip' and answer['filename']==image:
            path = self._file_storage_path(answer['sha1'], image)
            return self.download(path, answer['mimetype'], image) 

        return self.download(path, mimetypes.guess_type(image)[0], image)


    def download(self, path, mime_type, filename, require_staff=False):
        """
        Return a file from storage and return in a Response.
        """
        try:
            #if self.get_storage_type()=='File':
            if self.get_storage_type()=='File':
        #    file_descriptor = default_storage.open(path)
                file_absolute_path=os.path.join(IMAGEDIFF_ROOT,path)
                file_descriptor =open(file_absolute_path,"r")
                app_iter = iter(partial(file_descriptor.read, BLOCK_SIZE), '')
            #elif self.get_storage_type()=='S3':
            elif self.get_storage_type()=='S3':  
               # file_descriptor = default_storage._open(path)
                fp=S3BotoStorageFile(path,'r',default_storage)
                app_iter = iter(partial(fp.read, BLOCK_SIZE), '')
           # file_descriptor.close()
            return Response(
                app_iter=app_iter,
                content_type=mime_type,
                content_disposition="attachment; filename=" + filename.encode('utf-8'))
        except IOError:
            if require_staff:
                return Response(
                    "Sorry, assignment {} cannot be found at"
                    " {}. Please contact {}".format(
                        filename.encode('utf-8'), path, settings.TECH_SUPPORT_EMAIL
                    ),
                    status_code=404
                )
            #return HttpResponseNotFound('<h1>File not found</h1>')
            #raise Http404
            #raise JsonHandlerError( 404, 'Sorry, assignment cannot be found.')

            return Response(
                "Sorry, the file you uploaded, {}, cannot be"
                " found. Please try uploading it again or contact"
                " course staff".format(filename.encode('utf-8')),
                status_code=404
            )
    
    def answer_available(self):
        """
        Is the user allowed to see an answer?
        """
        if self.showanswer == '':
            return False
        elif self.showanswer == SHOWANSWER.NEVER:
            return False
        elif self.xmodule_runtime.user_is_staff:
            # This is after the 'never' check because admins can see the answer
            # unless the problem explicitly prevents it
            return True
        elif self.showanswer == SHOWANSWER.ATTEMPTED:
            return self.attempts > 0
        #elif self.showanswer == SHOWANSWER.ANSWERED:
            # NOTE: this is slightly different from 'attempted' -- resetting the problems
            # makes lcp.done False, but leaves attempts unchanged.
        #    return self.done
        elif self.showanswer == SHOWANSWER.CLOSED:
            return self.closed()
        #Show the answer after learner has answered the problem correctly, learner has no attempts left, or the problem due date has passed.
        elif self.showanswer == SHOWANSWER.FINISHED:
            return self.closed() or self.is_correct()

        elif self.showanswer == SHOWANSWER.CORRECT_OR_PAST_DUE:
            return self.is_correct() or self.is_past_due()
        elif self.showanswer == SHOWANSWER.PAST_DUE:
            return self.is_past_due()
        elif self.showanswer == SHOWANSWER.ALWAYS:
            return True

        return False
 
    
    
    def is_past_due(self):
        """
        Is it now past this problem's due date
        """
        return (get_extended_due_date(self) is not None and
                datetime.datetime.now(UTC()) >  get_extended_due_date(self))

    def closed(self):
        """
        Is the student still allowed to submit answers?
        """
        """
        Closed means either it is past due or stduent has made maximum attempts allowed.
        """
        if self.max_attempts != 100000000 and self.attempts >= self.max_attempts:
            return True
        if self.is_past_due():
            return True

    def is_course_staff(self):
        # pylint: disable=no-member
        """
         Check if user is course staff.
        """
        return getattr(self.xmodule_runtime, 'user_is_staff', False)
    
    def is_correct(self):
        """
         Check if all images passed
        """
        if self.image_passed==self.max_score():
            return True
        else:
            return False

    def is_instructor(self):
        # pylint: disable=no-member
        """
        Check if user role is instructor.
        """
        return self.xmodule_runtime.get_user_role() == 'instructor'

    def show_staff_grading_interface(self):
        """
        Return if current user is staff and not in studio.
        """
        in_studio_preview = self.scope_ids.user_id is None
        return self.is_course_staff() and not in_studio_preview


    # IITBombayX zip
    def _file_storage_unzip_and_diff_path_part(self, sha1, action,watermark=False):
        # pylint: disable=no-member
        """
        Get file unzip or difference-image path of storage.
        """
        if action and not watermark:  
            path_part = (
                '{loc.org}/{loc.course}/{action}/{user_id_seed}/{loc.block_type}'
                '/{loc.block_id}/{sha1}'.format(
                    loc=self.location,
                    action=action,
                    user_id_seed=self.xmodule_runtime.user_id,
                    sha1=sha1
                )
            )
        elif action and watermark:
            path_part = (
                '{loc.org}/{loc.course}/{action}/{loc.block_type}'
                '/{loc.block_id}/{sha1}'.format(
                    loc=self.location,
                    action=action,
                    sha1=sha1
                )
            )

        return path_part
    
    def delete_old_files(self, sha1):
        path_to_delete = (
                '{loc.org}/{loc.course}/unzip/{user_id_seed}/{loc.block_type}'
                '/{loc.block_id}/'.format(
                    loc=self.location,
                    user_id_seed=self.xmodule_runtime.user_id
                    
                )
            )
        fullpath_to_del = os.path.join(IMAGEDIFF_ROOT,path_to_delete)

        for x in os.listdir(fullpath_to_del):
            if (x != sha1):
                shutil.rmtree(os.path.join(fullpath_to_del,x))



    # IITBombayX zip 
    def _file_storage_unzip_and_diff_path(self, sha1, action, filename):
        # pylint: disable=no-member
        """
        Get file unzip or difference-image path of storage.
        """
        if action and filename:
            path = (
                '{loc.org}/{loc.course}/{action}/{user_id_seed}/{loc.block_type}'
                '/{loc.block_id}/{sha1}/{filename}'.format(
                    loc=self.location,
                    action=action,
                    user_id_seed=self.xmodule_runtime.user_id,
                    sha1=sha1,
                    filename=filename
                )
            )
        return path

    def _create_solution_image_path_from_http_url(self,afacultyfileurl,facfile):
        """
        Copies the solution image uploaded as asset by faculty and returns the absolute path of the same.
        """
        solution_part_path=(
            '{loc.org}/{loc.course}/solution/{loc.block_type}'
             '/{loc.block_id}/'.format(
                 loc=self.location
                    
                )
            )
       # if self.get_storage_type()=='File':
        fac_solution_dir=os.path.join(IMAGEDIFF_ROOT,solution_part_path)
        file_to_copy=os.path.join(fac_solution_dir,facfile) if self.get_storage_type()=='File' else os.path.join(solution_part_path,facfile) if self.get_storage_type()=='S3' else None
        if  self.get_storage_type()=='File' and os.path.exists(file_to_copy):
            return file_to_copy
        elif self.get_storage_type()=='S3' and default_storage.exists(file_to_copy):
            return file_to_copy
        if not os.path.exists(fac_solution_dir):
            os.makedirs(fac_solution_dir)
          
        try:
            #IITBombayX- Added check for supporting both course formats(draft and split) 
            afacultyfileurl=afacultyfileurl.split('/')[1] if "asset-v1:" in afacultyfileurl else afacultyfileurl
            asset_key = AssetKey.from_string(afacultyfileurl) if afacultyfileurl else None
            content=contentstore().find(asset_key)
        except Exception as e:
            log.info(e)
            return None
        CHUNK =1024
        file_to_copy=os.path.join(fac_solution_dir,facfile)
	try:
            with open(file_to_copy, 'wb') as f:
                try:
                    f.write(content.data)
                    f.close()
                    if self.get_storage_type()=='S3':
                        file_name=os.path.join(solution_part_path,facfile)
                        file_content=open(file_to_copy,'r')
                        save_file(file_name,File(file_content))
                        file_content.close()
                        os.remove(file_to_copy)
		except Exception as e:
                     log.info(e)
		     f.close()
            #f.close()
	except Exception as e:
                log.info(e)
        return file_to_copy if self.get_storage_type()=='File' else os.path.join(solution_part_path,facfile) if self.get_storage_type()=='S3' else None



    def _file_storage_path(self, sha1, filename):
        # pylint: disable=no-member
        """
        Get file path of storage.
        """
        path = (
            '{loc.org}/{loc.course}/{loc.block_type}/{loc.block_id}'
            '/{sha1}{ext}'.format(
                loc=self.location,
                sha1=sha1,
                ext=os.path.splitext(filename)[1]
            )
        )
        return path


def _get_sha1(file_descriptor):
    """
    Get file hex digest (fingerprint).
    """
    sha1 = hashlib.sha1()
    for block in iter(partial(file_descriptor.read, BLOCK_SIZE), ''):
        sha1.update(block)
    file_descriptor.seek(0)
    return sha1.hexdigest()


def _resource(path):  # pragma: NO COVER
    """
    Handy helper for getting resources from our kit.
    """
    data = pkg_resources.resource_string(__name__, path)
    return data.decode("utf8")


def _now():
    """
    Get current date and time.
    """
    return datetime.datetime.utcnow().replace(tzinfo=pytz.utc)


def load_resource(resource_path):  # pragma: NO COVER
    """
    Gets the content of a resource
    """
    resource_content = pkg_resources.resource_string(__name__, resource_path)
    return unicode(resource_content)


def render_template(template_path, context=None):  # pragma: NO COVER
    """
    Evaluate a template by resource path, applying the provided context.
    """
    if context is None:
        context = {}

    template_str = load_resource(template_path)
    template = Template(template_str)
    return template.render(Context(context))


def require(assertion):
    """
    Raises PermissionDenied if assertion is not true.
    """
    if not assertion:
        raise PermissionDenied



