import json

from django.contrib.auth.models import User
from courseware.models import StudentModule
import logging

log = logging.getLogger(__name__)

#grades.student_grade_save(self.course_id, self.xmodule_runtime.user_id, self.location)

def student_grade_save(course_id, student_id, location):
    """
    Save student grade
    """
    try:

        course_key = course_id
        usage_key = course_key.make_usage_key(location.block_type, location.block_id)
        state_csm = None
        student = User.objects.get(id=int(student_id))
        student_module = StudentModule.objects.get(
            course_id=course_key,
            module_state_key=usage_key,
            student_id=student.id
        )

        if student_module and student_module.pk:
            state_csm = json.loads(student_module.state)
            log.info(state_csm)

        if state_csm and 'points_earned' in state_csm and 'points_possible' in state_csm:
            student_module.max_grade = state_csm.get('points_possible', None)
            log.info(student_module.max_grade)
            student_module.grade = state_csm.get('points_earned', None)
            student_module.save()  

    except User.DoesNotExist:
        raise
    except StudentModule.DoesNotExist:
        raise
    except:
        raise


