//Abhishek
//var imported = document.createElement('script');
//imported.src = "/js/src/jquery.min.js";
//document.head.appendChild(imported);

//alert("hello");
//alert(imported.src);





function ImageDiffXBlock(runtime, element, server) {
    var saveUrl = runtime.handlerUrl(element, 'save_imagediffconfig');

    var validators = {
        'number': function(x) {
            return Number(x);
        },
        'string': function(x) {
            return !x ? null : x;
        }
    };

    function save() {
        var view = this;
        view.runtime.notify('save', {state: 'start'});

        var data = {};
        $(element).find('input').each(function(index, input) {
            data[input.name] = input.value;
        });
//IITBombayX
        $(element).find('select').each(function(index, input) {
            data[input.name] = input.value;
        });
       filearr=[];
       $("div.wrapper-comp-setting").find('div').each(function(index, input) {
           var pair =[];
           $(input).find('input').each(function(index,input){
                   pair.push(input.value)});
       filearr.push(pair); });
       data["filearr"]=filearr;


        $.ajax({
            type: 'POST',
            url: saveUrl,
            data: JSON.stringify(data),
            success: function() {
                view.runtime.notify('save', {state: 'end'});
		location.reload();
            }
        });
    }

    return {
        save: save
    };
}

