from PIL import Image
from PIL import ImageChops
from PIL import ImageFont
from PIL import ImageDraw
import math, operator

import os
import logging
from django.conf import settings
from django.core.files.storage import default_storage
from lms.envs import aws
from django.core.files import File
from storages.backends.s3boto import S3BotoStorageFile
logging.basicConfig()
log=logging.getLogger(__name__)
IMAGEDIFF_ROOT = "/edx/var/edxapp/uploads/"

#image difference function
def imagecomp (facultyimage, studentimage, diffimage,watermarkimage):
    if get_storage_type()=='File':
        i1 = Image.open(facultyimage)#Faculty image
        i2 = Image.open(studentimage)#Student image
    elif get_storage_type()=='S3':
        #fpF=default_storage._open(facultyimage)
        #fpS=default_storage._open(studentimage)
        fpF=S3BotoStorageFile(facultyimage,'r',default_storage)
        fpS=S3BotoStorageFile(studentimage,'r',default_storage)
        i1=Image.open(fpF.file)
        i2=Image.open(fpS.file)

    width, height = i1.size #Getting the width and height of image
    total_pixel = width*height #total pixel count


    x = i1.load()#Loading the Faculty image into a variable
    y = i2.load()#Loading the Student image into a variable

#If the difference image name input as parameter is not JPEG, then give error! This is a requirement

    f, extn = os.path.splitext(diffimage)
    if(i1.size != i2.size): #if the file types are different, give 0 score
        i1.close()
        i2.close()
        #Even if file size of student uploaded image is not correct, it should still show solution image 
        createwatermark(facultyimage,watermarkimage)
	return (None, None, total_pixel)

    #############Calculating pixel difference#################

    #width, height = i1.size #Getting the width and height of image

    i = 0
    j = 0
    c = 0

    #While loop to calculate the pixel difference
    while i<height:
    	j = 0
    	while j<width:
		if x[j,i]!= y[j,i]: #Comparing the pixel of Faculty and Student image
			c = c+1 #Pixel counter
    		j = j+1
	i = i+1

    ###############Calculating the percentage difference################

    p = round(((c*100)/float(width*height)),2)


	
    #watermark text on image
    text = "IITBombayX" #Watermark text
    tcolor = (255,0,0) #text color of watermark
    #i3 = Image.open("/home/edx/image/image4.jpg") #Image uploaded by faculty
    if get_storage_type()=='File':
        i3 = Image.open(facultyimage) #Image uploaded by faculty
    elif get_storage_type()=='S3':
        #fp=default_storage._open(facultyimage)
        fp=S3BotoStorageFile(facultyimage,'r',default_storage)
        log.info("Before checking type of object")
        imgdata = fp.file
        log.info("Checking type of object")
        log.info(imgdata)
        i3=Image.open(imgdata)
    width, height = i1.size #finding thr image size
    if(width>=960.0): #checking the image size for font size

	#if font are not loaded for the given file path it will load the default font
	try:
		font = ImageFont.truetype("/edx/var/edxapp/staticfiles/fonts/OpenSans/OpenSans-Regular-webfont.ttf",40)
	except:
		font = ImageFont.load_default()
    else:
	try:
		font = ImageFont.truetype("/edx/var/edxapp/staticfiles/fonts/OpenSans/OpenSans-Regular-webfont.ttf",24)
	except:
		font = ImageFont.load_default()


    text_pos1 = (0.0, 0.0) #text position one for watermark
    text_pos2 = ((width/2), (height/2)) #text position two for watermark
    draw = ImageDraw.Draw(i3) #draw function to create the new image
    draw.text(text_pos1, text, fill=tcolor, font=font) # draw.text to write text over the new image
    draw.text(text_pos2, text, fill=tcolor, font=font) # draw.text to write text over the new image
    del draw
    if get_storage_type()=='File':
        i3.save(watermarkimage) #save the new watermark image 
    elif get_storage_type()=='S3':
        localimage=os.path.join(IMAGEDIFF_ROOT,watermarkimage)
        i3.save(localimage)
        s3image=open(localimage,'r')
        default_storage._save(watermarkimage,File(s3image))
        s3image.close()
        os.remove(localimage)
    ###Saving the difference image##############
    try:
        diff = ImageChops.difference(i1.convert('RGB'), i2.convert('RGB'))#calculating the difference image
    except:
        i1.close()
        i2.close()
        return None, None, total_pixel
    if get_storage_type()=='File':
        diff.save(diffimage,"JPEG")#saving the difference image
    elif get_storage_type()=='S3':
        # cannot specify format in s3 as here it is explicitily saved as JPEG
        localdiffimage=os.path.join(IMAGEDIFF_ROOT,diffimage)
        diff.save(localdiffimage,"JPEG")
        s3diffimage=open(localdiffimage,'r')
        default_storage._save(diffimage,File(s3diffimage))
        s3diffimage.close()
        os.remove(localdiffimage)
    i1.close()
    i2.close()
    i3.close()
    

    return (p, c, total_pixel) #return difference in pixel and percentage
def createwatermark(facultyimage,watermarkimage):
  #watermark text on image
    text = "IITBombayX" #Watermark text
    tcolor = (255,0,0) #text color of watermark
    #i3 = Image.open("/home/edx/image/image4.jpg") #Image uploaded by faculty
    if get_storage_type()=='File':
        i3 = Image.open(facultyimage) #Image uploaded by faculty
    elif get_storage_type()=='S3':
        #fp=default_storage._open(facultyimage)
        fp=S3BotoStorageFile(facultyimage,'r',default_storage)
        log.info("CW Before Checking type of object")
        imgdata = fp.file
        log.info("CW Checking type of object")
        i3=Image.open(imgdata)
    
    width, height = i3.size #finding thr image size
    if(width>=960.0): #checking the image size for font size

        #if font are not loaded for the given file path it will load the default font
        try:
                font = ImageFont.truetype("/edx/var/edxapp/staticfiles/fonts/OpenSans/OpenSans-Regular-webfont.ttf",40)
        except:
                font = ImageFont.load_default()
    else:
        try:
                font = ImageFont.truetype("/edx/var/edxapp/staticfiles/fonts/OpenSans/OpenSans-Regular-webfont.ttf",24)
        except:
                font = ImageFont.load_default()


    text_pos1 = (0.0, 0.0) #text position one for watermark
    text_pos2 = ((width/2), (height/2)) #text position two for watermark
    draw = ImageDraw.Draw(i3) #draw function to create the new image
    draw.text(text_pos1, text, fill=tcolor, font=font) # draw.text to write text over the new image
    draw.text(text_pos2, text, fill=tcolor, font=font) # draw.text to write text over the new image
    del draw
    if get_storage_type()=='File':
        i3.save(watermarkimage) #save the new watermark image
        i3.close()
    elif get_storage_type()=='S3':
        localimage=os.path.join(IMAGEDIFF_ROOT,watermarkimage)
        i3.save(localimage)
        s3image=open(localimage,'r')
        default_storage._save(watermarkimage,File(s3image))
        s3image.close()
        os.remove(localimage)
        i3.close()

def get_storage_type():
    return aws.ENV_TOKENS.get('FILE_STORE') if aws.ENV_TOKENS.get('FILE_STORE') else 'File'
if __name__ == "__main__":
    im1 = "/home/edx/image/image3.jpg"#Image uploaded by user
    im2 = "/home/edx/image/image4.jpg"#Image uploaded by faculty
    im3 = "/home/edx/image/diff.jpg"#Image uploaded by faculty
    im4 = "/home/edx/image/watermark.jpg"#Image uploaded by faculty
    a = 0.0
    b = 0.0
    c = 0.0
    a, b, c = imagecomp(im1, im2, im3, im4) #function call

    if(a==b==None): #checking the return value
        print "Differnt sized/type images! Cannot compare"
    else:
        print "Difference (Percentage):", a
        print "Difference (Pixel):", b
        print "Total (Pixel):", c

